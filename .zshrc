autoload -Uz compinit promptinit

compinit
zstyle ':completion:*' menu select
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

promptinit
prompt redhat

